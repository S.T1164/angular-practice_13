import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

class MyData{
  data : string;
  list : Person[] = [];
}

class Person{
  name : string;
  mail : string;
  tel : string;
}

@Injectable({
  providedIn: 'root'
})
export class MycheckService {

  private myData : MyData = new MyData();

  constructor(private client : HttpClient) {
    this.client.get('/assets/data.json')
    .subscribe((result : MyData) => {
      this.myData = result;
    }); 
   }

  get(n : number){
    return this.myData.list[n];
  }

  get size(){
    return this.list.length;
  }

  get data(){
    return this.myData.data;
  }

  get list(){
    return this.myData.list;
  }
  
}
